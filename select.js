db.Event.find({ "Date": { $gt: ISODate("2024-08-01T00:00:00Z") } });

db.Guest.find({ 
    $or: [
        { "Membership": "VIP" },
        { "Membership": "Friend" }
    ]
});

db.Table.find({ 
    "Capacity": { $gt: 8 },
    "Reservations.Reservation status": "Paid"
});

db.Event.find({ "Name": /Techno/ });

db.Guest.find({}, { "_id": 1, "Date of birth": 1 });

db.Table.find({}, { "_id": 0, "Number": 1, "Capacity": 1 });
