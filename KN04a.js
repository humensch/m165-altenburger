db.Table.aggregate([
    { $match: { "Capacity": { $gt: 8 } } },
    { $match: { "Reservations.Reservation status": "Paid" } }
]);

db.Event.aggregate([
    { $match: { "Date": { $gt: ISODate("2024-08-01T00:00:00Z") } } },
    { $project: { _id: 0, Name: 1, Date: 1 } },
    { $sort: { Date: 1 } }
]);

db.Guest.aggregate([
    { $count: "Guest count:" }
]);

//Gleiche Funktionsweise wie die vorherige Funktion, jedoch nach Entry fee gruppiert
//https://www.mongodb.com/docs/manual/reference/operator/aggregation/count/
db.Event.aggregate([
    { $group: { _id: "$Entryfee", eventCount: { $sum: 1 } } },
    { $project: { _id: 0 }}
]);
