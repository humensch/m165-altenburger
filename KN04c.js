db.Guest.aggregate([
    { $unwind: "$Tickets" },
    { $project: { _id: 0, "Tickets": 1 } }
]);

db.Guest.aggregate([
    { $unwind: "$Tickets" },
    { $match: { "Tickets.Event": "Neon Pulse" } },
    { $project: { _id: 0, "Date of birth": 1, "Membership": 1, "Tickets.Event": 1, "Tickets.Date purchased": 1 } }
]);

db.Guest.aggregate([
    { $unwind: "$Tickets" },
    { $project: { _id: 0, "Date of birth": 1, "Membership": 1, "Tickets.Event": 1, "Tickets.Date purchased": 1 } }
]);
