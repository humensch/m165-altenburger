var EventEclipse = new ObjectID();
var EventVortex = new ObjectID();
var EventPulse = new ObjectID();

db.Event.insertOne({
    _id: EventEclipse,
    "Name": "Eclipse Techno Night",
    "Date": ISODate("2024-07-09T00:00:00Z"),
    "Entryfee": 20
});

db.Event.insertOne({
    _id: EventVortex,
    "Name": "Techno Vortex",
    "Date": ISODate("2024-08-25T00:00:00Z"),
    "Entryfee": 40
});

db.Event.insertOne({
    _id: EventPulse,
    "Name": "Neon Pulse",
    "Date": ISODate("2024-09-18T00:00:00Z"),
    "Entryfee": 35
});

var GuestTenzin = new ObjectID();
var GuestJoa = new ObjectID();
var GuestTimon = new ObjectID();

db.Guest.insertMany([
    {
        _id: GuestTenzin,
        "Date of birth": ISODate("2003-03-25T00:00:00Z"),
        "Membership": "Member",
        "Tickets": [
            {
                "EventID": EventEclipse,
                "Event": "Eclipse Techno Night",
                "Date purchased": ISODate("2024-06-25T00:00:00Z")
            },
            {
                "EventID": EventVortex,
                "Event": "Techno Vortex",
                "Date purchased": ISODate("2024-06-26T00:00:00Z")
            }
        ]
    },
    {
        _id: GuestJoa,
        "Date of birth": ISODate("2005-05-16T00:00:00Z"),
        "Membership": "VIP",
        "Tickets": [
            {
                "EventID": EventVortex,
                "Event": "Techno Vortex",
                "Date purchased": ISODate("2024-05-12T00:00:00Z")
            },
            {
                "EventID": EventPulse,
                "Event": "Neon Pulse",
                "Date purchased": ISODate("2024-05-18T00:00:00Z")
            }
        ]
    },
    {
        _id: GuestTimon,
        "Date of birth": ISODate("2002-03-18T00:00:00Z"),
        "Membership": "Friend",
        "Tickets": [
            {
                "EventID": EventEclipse,
                "Event": "Eclipse Techno Night",
                "Date purchased": ISODate("2024-06-19T00:00:00Z")
            },
            {
                "EventID": EventPulse,
                "Event": "Neon Pulse",
                "Date purchased": ISODate("2024-06-25T00:00:00Z")
            }
        ]
    }
]);

var Table1 = new ObjectID();
var Table2 = new ObjectID();
var Table3 = new ObjectID();

db.Table.insertMany([
    {
        _id: Table1,
        "Number": 1,
        "Capacity": 6,
        "Reservations": [
            {
                "By": GuestJoa,
                "Reservation status": "Paid",
                "Date": ISODate("2024-08-25T00:00:00Z")
            },
            {
                "By": GuestTimon,
                "Reservation status": "Reserved",
                "Date": ISODate("2024-09-18T00:00:00Z")
            }
        ]
    },
    {
        _id: Table2,
        "Number": 2,
        "Capacity": 10,
        "Reservations": [
            {
                "By": GuestTenzin,
                "Reservation status": "Paid",
                "Date": ISODate("2024-07-09T00:00:00Z")
            },
            {
                "By": GuestJoa,
                "Reservation status": "Reserved",
                "Date": ISODate("2024-09-18T00:00:00Z")
            }
        ]
    },
    {
        _id: Table3,
        "Number": 3,
        "Capacity": 12,
        "Reservations": [
            {
                "By": GuestTimon,
                "Reservation status": "Paid",
                "Date": ISODate("2024-07-09T00:00:00Z")
            },
            {
                "By": GuestTenzin,
                "Reservation status": "Paid",
                "Date": ISODate("2024-08-25T00:00:00Z")
            }
        ]
    }
]);
