db.Event.updateOne(
    { _id: ObjectId("EventEclipse") }, 
    { $set: { "Entryfee": 25 } }
);

db.Guest.updateMany(
    { 
        $or: [
            { "Membership": "Member" },
            { "Membership": "Friend" }
        ]
    },
    { $set: { "Membership": "Regular" } }
);

db.Table.replaceOne(
    { "Number": 2 }, 
    {
        "Number": 2,
        "Capacity": 8,
        "Reservations": [
            {
                "By": ObjectId("GuestTenzin"),
                "Reservation status": "Paid",
                "Date": ISODate("2024-07-09T00:00:00Z")
            },
            {
                "By": ObjectId("GuestJoa"),
                "Reservation status": "Reserved",
                "Date": ISODate("2024-09-18T00:00:00Z")
            }
        ]
    }
);
