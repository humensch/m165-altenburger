db.Guest.aggregate([
    {
        $lookup: {
            from: "Event",
            localField: "Tickets.EventID",
            foreignField: "_id",
            as: "EventDetails"
        }
    },
    {
        $project: {
            _id: 1,
            "Date of birth": 1,
            "Membership": 1,
            "Tickets": 1,
            "EventDetails": { "Name": 1, "Date": 1, "Entryfee": 1 }
        }
    }
]);

db.Guest.aggregate([
    {
        $lookup: {
            from: "Table",
            localField: "_id",
            foreignField: "Reservations.By",
            as: "TableReservations"
        }
    },
    {
        $unwind: "$TableReservations"
    },
    {
        $match: {
            "TableReservations.Reservation status": "Paid"
        }
    },
    {
        $group: {
            _id: "$_id",
            "Name": { $first: "$_id" },
            "TotalPaidReservations": { $sum: 1 }
        }
    }
]);

db.Guest.aggregate([
    {
        $lookup: {
            from: "Event",
            localField: "Tickets.EventID",
            foreignField: "_id",
            as: "EventDetails"
        }
    },
    {
        $unwind: "$EventDetails"
    },
    {
        $match: {
            "EventDetails.Entryfee": { $gte: 30 }
        }
    },
    {
        $project: {
            _id: 1,
            "Date of birth": 1,
            "Membership": 1,
            "EventDetails.Name": 1,
            "EventDetails.Date": 1,
            "EventDetails.Entryfee": 1
        }
    }
]);
